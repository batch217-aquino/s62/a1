import axios from "axios";

const BASE_URL = "http://localhost:5000/api/";
// const TOKEN =
//   JSON.parse(JSON.parse(localStorage.getItem("persist:root")).user).currentUser
//     .accessToken || "";

const user = JSON.parse(localStorage.getItem("persist:root"))?.user;
const currentUser = user && JSON.parse(user).currentUser;
const TOKEN = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYzNzQ5ODJhNjk1MTIyZDFlMGM4YzMyMCIsImlzQWRtaW4iOnRydWUsImlhdCI6MTY2ODY2NTgyOSwiZXhwIjoxNjY4OTI1MDI5fQ.j8EpgMJDkiEcjAoTXbR2-VlRqPEdNksFHmeThrwdMNE";

export const publicRequest = axios.create({
  baseURL: BASE_URL,
});

export const userRequest = axios.create({
  baseURL: BASE_URL,
  header: { token: `Bearer ${TOKEN}` },
});

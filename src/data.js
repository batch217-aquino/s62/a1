export const sliderItems = [
  {
    id: 1,
    img: "https://consumer.huawei.com/content/dam/huawei-cbg-site/me-africa/common/mkt/press/news/2021/news-210605/list-img.jpg",
    title: "CHRISTMAS SALE!",
    desc: "UP TO 50% OFF.",
    bg: "FFFFFF",
  },
  {
    id: 2,
    img: "https://www.huaweicentral.com/wp-content/uploads/2020/12/huawei-watch-gt-2-pro-featured-img-1-1000x600.jpg",
    title: "HUAWEI Watches",
    desc: " UP TO 30% OFF FOR NEW ARRIVALS.",
    bg: "FFFFFF",
  },
  {
    id: 3,
    img: "https://consumer.huawei.com/content/dam/huawei-cbg-site/en/support/laptop-user-guide/img/laptop.png",
    title: "HUAWEI Laptops",
    desc: "UP TO 30% OFF FOR NEW ARRIVALS.",
    bg: "FFFFFF",
  },
];

export const categories = [
  {
    id: 1,
    img: "https://media.wired.com/photos/5aa84b605d79c76b98749cfd/master/pass/Surface-device-family-SOURCE-Microsoft-web.jpg",
    title: "LAPTOP",
    cat:"laptop"
  },
  {
    id: 2,
    img: "https://images.anandtech.com/doci/9770/X-T30_DSF5320_678x452.jpg",
    title: "SMARTPHONE",
    cat:"smartphone"
  },
  {
    id: 3,
    img: "https://www.91-cdn.com/hub/wp-content/uploads/2021/01/Smartwatch-with-SpO2.jpeg",
    title: "SMARTWATCH",
    cat:"smartwatch"
  },
];

export const popularProducts = [
  {
    id:1,
    img:"https://www.siliconvalley.com.ph/wp-content/uploads/2020/03/2020-10-11.png",
  },
  {
    id:2,
    img:"https://static-ecpa.acer.com/media/catalog/product/cache/ab3054ca0c987098b5bb23b2f6bbf813/n/i/nitro5_an515-57_bl_bk_modelmain_5.png",
  },
  {
    id:3,
    img:"https://asset.msi.com/resize/image/global/product/product_1652084089f186edc14f9d018b77a37b5aee4f5a5a.png62405b38c58fe0f07fcef2367d8a9ba1/400.png",
  },
  {
    id:4,
    img:"https://consumer.huawei.com/content/dam/huawei-cbg-site/common/mkt/pdp/admin-image/phones/mate50-pro/list/mate50-pro-black.png",
  },
  {
    id:5,
    img:"https://consumer.huawei.com/content/dam/huawei-cbg-site/common/mkt/list-image/phones/p40-pro/p40-pro-silver.png",
  },
  {
    id:6,
    img:"https://image01.realme.net/general/20220829/1661751475307.png",
  },
  {
    id:7,
    img:"https://consumer.huawei.com/content/dam/huawei-cbg-site/common/mkt/pdp/wearables/watch-3/active-edition.png",
  },
  {
    id:8,
    img:"https://consumer.huawei.com/content/dam/huawei-cbg-site/common/mkt/pdp/wearables/watch-fit/dynamic/watch-fit/img/pc/huawei-watch-fit-personal-assistant-3.jpg",
  },
]